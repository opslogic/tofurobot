#!/bin/bash

for I in
	docker ps -a | egrep ghost | egrep -v egrep | awk {'print $1'}
do
	rm $I
done
